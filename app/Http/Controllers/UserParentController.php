<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserParentRequest;
use App\Http\Requests\UpdateUserParentRequest;
use App\Models\UserParent;

class UserParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserParentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserParentRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserParent  $userParent
     * @return \Illuminate\Http\Response
     */
    public function show(UserParent $userParent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserParent  $userParent
     * @return \Illuminate\Http\Response
     */
    public function edit(UserParent $userParent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserParentRequest  $request
     * @param  \App\Models\UserParent  $userParent
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserParentRequest $request, UserParent $userParent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserParent  $userParent
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserParent $userParent)
    {
        //
    }
}
