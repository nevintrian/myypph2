<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookBadgeRequest;
use App\Http\Requests\UpdateBookBadgeRequest;
use App\Models\BookBadge;

class BookBadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookBadgeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookBadgeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookBadge  $BookBadge
     * @return \Illuminate\Http\Response
     */
    public function show(BookBadge $BookBadge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookBadge  $BookBadge
     * @return \Illuminate\Http\Response
     */
    public function edit(BookBadge $BookBadge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookBadgeRequest  $request
     * @param  \App\Models\BookBadge  $BookBadge
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookBadgeRequest $request, BookBadge $BookBadge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookBadge  $BookBadge
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookBadge $BookBadge)
    {
        //
    }
}
