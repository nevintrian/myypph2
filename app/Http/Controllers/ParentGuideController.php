<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreParentGuideRequest;
use App\Http\Requests\UpdateParentGuideRequest;
use App\Models\ParentGuide;

class ParentGuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreParentGuideRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreParentGuideRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ParentGuide  $parentGuide
     * @return \Illuminate\Http\Response
     */
    public function show(ParentGuide $parentGuide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ParentGuide  $parentGuide
     * @return \Illuminate\Http\Response
     */
    public function edit(ParentGuide $parentGuide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateParentGuideRequest  $request
     * @param  \App\Models\ParentGuide  $parentGuide
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateParentGuideRequest $request, ParentGuide $parentGuide)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ParentGuide  $parentGuide
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentGuide $parentGuide)
    {
        //
    }
}
