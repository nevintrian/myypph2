<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChildQuizRequest;
use App\Http\Requests\UpdateChildQuizRequest;
use App\Models\ChildQuiz;

class ChildQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreChildQuizRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChildQuizRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChildQuiz  $childQuiz
     * @return \Illuminate\Http\Response
     */
    public function show(ChildQuiz $childQuiz)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChildQuiz  $childQuiz
     * @return \Illuminate\Http\Response
     */
    public function edit(ChildQuiz $childQuiz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateChildQuizRequest  $request
     * @param  \App\Models\ChildQuiz  $childQuiz
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChildQuizRequest $request, ChildQuiz $childQuiz)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChildQuiz  $childQuiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChildQuiz $childQuiz)
    {
        //
    }
}
