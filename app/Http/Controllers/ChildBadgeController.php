<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChildBadgeRequest;
use App\Http\Requests\UpdateChildBadgeRequest;
use App\Models\ChildBadge;

class ChildBadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreChildBadgeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChildBadgeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChildBadge  $childBadge
     * @return \Illuminate\Http\Response
     */
    public function show(ChildBadge $childBadge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChildBadge  $childBadge
     * @return \Illuminate\Http\Response
     */
    public function edit(ChildBadge $childBadge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateChildBadgeRequest  $request
     * @param  \App\Models\ChildBadge  $childBadge
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChildBadgeRequest $request, ChildBadge $childBadge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChildBadge  $childBadge
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChildBadge $childBadge)
    {
        //
    }
}
