<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookCollectionRequest;
use App\Http\Requests\UpdateBookCollectionRequest;
use App\Models\BookCollection;

class BookCollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookCollectionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookCollectionRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookCollection  $bookCollection
     * @return \Illuminate\Http\Response
     */
    public function show(BookCollection $bookCollection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookCollection  $bookCollection
     * @return \Illuminate\Http\Response
     */
    public function edit(BookCollection $bookCollection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookCollectionRequest  $request
     * @param  \App\Models\BookCollection  $bookCollection
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookCollectionRequest $request, BookCollection $bookCollection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookCollection  $bookCollection
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookCollection $bookCollection)
    {
        //
    }
}
