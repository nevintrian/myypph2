<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChildAssignmentRequest;
use App\Http\Requests\UpdateChildAssignmentRequest;
use App\Models\ChildAssignment;

class ChildAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreChildAssignmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChildAssignmentRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChildAssignment  $childAssignment
     * @return \Illuminate\Http\Response
     */
    public function show(ChildAssignment $childAssignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChildAssignment  $childAssignment
     * @return \Illuminate\Http\Response
     */
    public function edit(ChildAssignment $childAssignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateChildAssignmentRequest  $request
     * @param  \App\Models\ChildAssignment  $childAssignment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChildAssignmentRequest $request, ChildAssignment $childAssignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChildAssignment  $childAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChildAssignment $childAssignment)
    {
        //
    }
}
