<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeacherGuideRequest;
use App\Http\Requests\UpdateTeacherGuideRequest;
use App\Models\TeacherGuide;

class TeacherGuideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTeacherGuideRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeacherGuideRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TeacherGuide  $teacherGuide
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherGuide $teacherGuide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TeacherGuide  $teacherGuide
     * @return \Illuminate\Http\Response
     */
    public function edit(TeacherGuide $teacherGuide)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTeacherGuideRequest  $request
     * @param  \App\Models\TeacherGuide  $teacherGuide
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeacherGuideRequest $request, TeacherGuide $teacherGuide)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TeacherGuide  $teacherGuide
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeacherGuide $teacherGuide)
    {
        //
    }
}
