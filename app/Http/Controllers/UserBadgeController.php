<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserBadgeRequest;
use App\Http\Requests\UpdateUserBadgeRequest;
use App\Models\UserBadge;

class UserBadgeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserBadgeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserBadgeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserBadge  $userBadge
     * @return \Illuminate\Http\Response
     */
    public function show(UserBadge $userBadge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserBadge  $userBadge
     * @return \Illuminate\Http\Response
     */
    public function edit(UserBadge $userBadge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserBadgeRequest  $request
     * @param  \App\Models\UserBadge  $userBadge
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserBadgeRequest $request, UserBadge $userBadge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserBadge  $userBadge
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserBadge $userBadge)
    {
        //
    }
}
