<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookContentTranslationRequest;
use App\Http\Requests\UpdateBookContentTranslationRequest;
use App\Models\BookContentTranslation;

class BookContentTranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookContentTranslationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookContentTranslationRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BookContentTranslation  $bookContentTranslation
     * @return \Illuminate\Http\Response
     */
    public function show(BookContentTranslation $bookContentTranslation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BookContentTranslation  $bookContentTranslation
     * @return \Illuminate\Http\Response
     */
    public function edit(BookContentTranslation $bookContentTranslation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookContentTranslationRequest  $request
     * @param  \App\Models\BookContentTranslation  $bookContentTranslation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookContentTranslationRequest $request, BookContentTranslation $bookContentTranslation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookContentTranslation  $bookContentTranslation
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookContentTranslation $bookContentTranslation)
    {
        //
    }
}
