<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAssignmentBookRequest;
use App\Http\Requests\UpdateAssignmentBookRequest;
use App\Models\AssignmentBook;

class AssignmentBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAssignmentBookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAssignmentBookRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssignmentBook  $assignmentBook
     * @return \Illuminate\Http\Response
     */
    public function show(AssignmentBook $assignmentBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssignmentBook  $assignmentBook
     * @return \Illuminate\Http\Response
     */
    public function edit(AssignmentBook $assignmentBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAssignmentBookRequest  $request
     * @param  \App\Models\AssignmentBook  $assignmentBook
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAssignmentBookRequest $request, AssignmentBook $assignmentBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssignmentBook  $assignmentBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignmentBook $assignmentBook)
    {
        //
    }
}
