<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChildBookRequest;
use App\Http\Requests\UpdateChildBookRequest;
use App\Models\ChildBook;

class ChildBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreChildBookRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreChildBookRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChildBook  $childBook
     * @return \Illuminate\Http\Response
     */
    public function show(ChildBook $childBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChildBook  $childBook
     * @return \Illuminate\Http\Response
     */
    public function edit(ChildBook $childBook)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateChildBookRequest  $request
     * @param  \App\Models\ChildBook  $childBook
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateChildBookRequest $request, ChildBook $childBook)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChildBook  $childBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChildBook $childBook)
    {
        //
    }
}
