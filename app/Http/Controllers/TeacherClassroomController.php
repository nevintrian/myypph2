<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeacherClassroomRequest;
use App\Http\Requests\UpdateTeacherClassroomRequest;
use App\Models\TeacherClassroom;

class TeacherClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTeacherClassroomRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeacherClassroomRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TeacherClassroom  $teacherClassroom
     * @return \Illuminate\Http\Response
     */
    public function show(TeacherClassroom $teacherClassroom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TeacherClassroom  $teacherClassroom
     * @return \Illuminate\Http\Response
     */
    public function edit(TeacherClassroom $teacherClassroom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTeacherClassroomRequest  $request
     * @param  \App\Models\TeacherClassroom  $teacherClassroom
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeacherClassroomRequest $request, TeacherClassroom $teacherClassroom)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TeacherClassroom  $teacherClassroom
     * @return \Illuminate\Http\Response
     */
    public function destroy(TeacherClassroom $teacherClassroom)
    {
        //
    }
}
