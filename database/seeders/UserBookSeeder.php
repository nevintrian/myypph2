<?php

namespace Database\Seeders;

use App\Models\UserBook;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserBook::create([
            'book_id' => 1,
            'user_id' => 1,
            'current_page' => 100,
            'is_done' => 1,
            'is_favorite' => 1
        ]);

        UserBook::create([
            'book_id' => 3,
            'user_id' => 1,
            'current_page' => 10,
            'is_done' => 0,
            'is_favorite' => 1
        ]);

        UserBook::create([
            'book_id' => 3,
            'user_id' => 2,
            'current_page' => 100,
            'is_done' => 1,
            'is_favorite' => 1,
            'rating' => 5
        ]);
    }
}
