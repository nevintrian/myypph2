<?php

namespace Database\Seeders;

use App\Models\ParentGuide;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParentGuideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ParentGuide::create([
            'title' => 'Petunjuk Orangtua 1',
            'content' => 'Ini adalah petunjuk 1'
        ]);
        ParentGuide::create([
            'title' => 'Petunjuk Orangtua 2',
            'content' => 'Ini adalah petunjuk 2'
        ]);
        ParentGuide::create([
            'title' => 'Petunjuk Orangtua 3',
            'content' => 'Ini adalah petunjuk 3'
        ]);
    }
}
