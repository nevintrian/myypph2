<?php

namespace Database\Seeders;

use App\Models\AssignmentBook;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AssignmentBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssignmentBook::create([
            'book_id' => 1,
            'assignment_id' => 1
        ]);
        AssignmentBook::create([
            'book_id' => 2,
            'assignment_id' => 1
        ]);

        AssignmentBook::create([
            'book_id' => 3,
            'assignment_id' => 2
        ]);
        AssignmentBook::create([
            'book_id' => 4,
            'assignment_id' => 2
        ]);
    }
}
