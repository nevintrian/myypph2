<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teacher::create([
            'user_id' => 3,
            'birth_date' => '1995-12-01'
        ]);
        Teacher::create([
            'user_id' => 4,
            'birth_date' => '1990-12-01'
        ]);
    }
}
