<?php

namespace Database\Seeders;

use App\Models\Activity;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activity::create([
            'child_book_id' => 1,
            'reading_time' => 5
        ]);
        Activity::create([
            'child_book_id' => 1,
            'reading_time' => 25
        ]);
        Activity::create([
            'child_book_id' => 1,
            'reading_time' => 15
        ]);
    }
}
