<?php

namespace Database\Seeders;

use App\Models\BookBadge;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookBadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookBadge::create([
            'book_id' => 1,
            'badge_id' => 1
        ]);

        BookBadge::create([
            'book_id' => 2,
            'badge_id' => 2
        ]);

        BookBadge::create([
            'book_id' => 2,
            'badge_id' => 3
        ]);

        BookBadge::create([
            'book_id' => 3,
            'badge_id' => 4
        ]);

        BookBadge::create([
            'book_id' => 3,
            'badge_id' => 5
        ]);

        BookBadge::create([
            'book_id' => 4,
            'badge_id' => 6
        ]);

        BookBadge::create([
            'book_id' => 4,
            'badge_id' => 7
        ]);
    }
}
