<?php

namespace Database\Seeders;

use App\Models\ChildQuiz;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChildQuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChildQuiz::create([
            'quiz_id' => 1,
            'child_id' => 1,
            'correct_answer' => 1,
            'wrong_answer' => 1,
            'score' => 50
        ]);
    }
}
