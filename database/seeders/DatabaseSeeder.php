<?php

namespace Database\Seeders;

use App\Models\BookBadge;
use App\Models\BookCollection;
use App\Models\ChildCategory;
use App\Models\ParentGuide;
use App\Models\QuizAnswer;
use App\Models\QuizQuestion;
use App\Models\TeacherClassroom;
use App\Models\TeacherGuide;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        // $this->call(AccountSeeder::class);
        $this->call(TypeSeeder::class);
        $this->call(AuthorSeeder::class);
        $this->call(AgeSeeder::class);
        $this->call(BookSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(BookCategorySeeder::class);
        $this->call(BookContentSeeder::class);
        $this->call(BookContentTranslationSeeder::class);
        // $this->call(UserCategorySeeder::class);
        // $this->call(UserBookSeeder::class);
        $this->call(BadgeSeeder::class);
        // $this->call(UserBadgeSeeder::class);
        $this->call(QuizSeeder::class);
        $this->call(QuizQuestionSeeder::class);
        $this->call(QuizAnswerSeeder::class);
        $this->call(BookBadgeSeeder::class);
        $this->call(TeacherSeeder::class);
        $this->call(SubscriptionSeeder::class);
        $this->call(UserParentSeeder::class);
        $this->call(ChildSeeder::class);
        $this->call(CollectionSeeder::class);
        $this->call(BookCollectionSeeder::class);
        $this->call(ClassroomSeeder::class);
        $this->call(SchoolSeeder::class);
        $this->call(StudentSeeder::class);
        $this->call(TeacherClassroomSeeder::class);
        $this->call(TransactionSeeder::class);
        $this->call(SearchSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(AssignmentSeeder::class);
        // $this->call(StudentAssignmentSeeder::class);
        $this->call(AssignmentBookSeeder::class);
        // $this->call(UserQuizSeeder::class);
        $this->call(SupportSeeder::class);
        // $this->call(TeacherGuideSeeder::class);
        // $this->call(ParentGuideSeeder::class);
        $this->call(FeedbackSeeder::class);
        $this->call(ChildBadgeSeeder::class);
        $this->call(ChildBookSeeder::class);
        $this->call(ChildCategorySeeder::class);
        $this->call(ChildAssignmentSeeder::class);
        $this->call(ChildQuizSeeder::class);
        $this->call(ActivitySeeder::class);
        $this->call(AdminSeeder::class);
    }
}
