<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
            'child_id' => 3,
            'school_id' => 1,
            'classroom_id' => 1,
            'student_number' => 'E12931',
            'code' => 'Apdjh'
        ]);

        Student::create([
            'child_id' => 4,
            'school_id' => 1,
            'classroom_id' => 1,
            'student_number' => 'E12932',
            'code' => 'Bqhhe'
        ]);
    }
}
