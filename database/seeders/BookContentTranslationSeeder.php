<?php

namespace Database\Seeders;

use App\Models\BookContentTranslation;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookContentTranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookContentTranslation::create([
            'book_content_id' => 1,
            'locale' => 'id',
            'text' => 'ini adalah book 1 page 1',
            'image' => 'book1_page1.jpg',
            'audio' => 'book1_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 1,
            'locale' => 'en',
            'text' => 'ini adalah book 1 page 2',
            'image' => 'book1_page2.jpg',
            'audio' => 'book1_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 2,
            'locale' => 'id',
            'text' => 'ini adalah book 2 page 1',
            'image' => 'book2_page1.jpg',
            'audio' => 'book2_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 2,
            'locale' => 'en',
            'text' => 'ini adalah book 2 page 2',
            'image' => 'book2_page2.jpg',
            'audio' => 'book2_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 3,
            'locale' => 'id',
            'text' => 'ini adalah book 3 page 1',
            'image' => 'book3_page1.jpg',
            'audio' => 'book3_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 3,
            'locale' => 'en',
            'text' => 'ini adalah book 3 page 2',
            'image' => 'book3_page2.jpg',
            'audio' => 'book3_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 4,
            'locale' => 'id',
            'text' => 'ini adalah book 4 page 1',
            'image' => 'book4_page1.jpg',
            'audio' => 'book4_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 4,
            'locale' => 'en',
            'text' => 'ini adalah book 4 page 2',
            'image' => 'book4_page2.jpg',
            'audio' => 'book4_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 5,
            'locale' => 'id',
            'text' => 'ini adalah book 5 page 1',
            'image' => 'book5_page1.jpg',
            'audio' => 'book5_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 5,
            'locale' => 'en',
            'text' => 'ini adalah book 5 page 2',
            'image' => 'book5_page2.jpg',
            'audio' => 'book5_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 6,
            'locale' => 'id',
            'text' => 'ini adalah book 6 page 1',
            'image' => 'book6_page1.jpg',
            'audio' => 'book6_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 6,
            'locale' => 'id',
            'text' => 'ini adalah book 6 page 2',
            'image' => 'book6_page2.jpg',
            'audio' => 'book6_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 7,
            'locale' => 'id',
            'text' => 'ini adalah book 7 page 1',
            'image' => 'book7_page1.jpg',
            'audio' => 'book7_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 7,
            'locale' => 'en',
            'text' => 'ini adalah book 7 page 2',
            'image' => 'book7_page2.jpg',
            'audio' => 'book7_page2.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 8,
            'locale' => 'id',
            'text' => 'ini adalah book 8 page 1',
            'image' => 'book8_page1.jpg',
            'audio' => 'book8_page1.mp3'
        ]);
        BookContentTranslation::create([
            'book_content_id' => 8,
            'locale' => 'en',
            'text' => 'ini adalah book 8 page 2',
            'image' => 'book8_page2.jpg',
            'audio' => 'book8_page2.mp3'
        ]);
    }
}
