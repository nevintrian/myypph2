<?php

namespace Database\Seeders;

use App\Models\Classroom;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classroom::create([
            'name' => 'Ini Kelas 1 SD',
            'grade' => '1',
        ]);

        Classroom::create([
            'name' => 'Ini Kelas 2 SD',
            'grade' => '2',
        ]);
    }
}
