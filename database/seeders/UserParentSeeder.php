<?php

namespace Database\Seeders;

use App\Models\UserParent;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserParentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserParent::create([
            'birth_year' => '2000',
            'user_id' => 1,
            'subscription_id' => 3
        ]);
        UserParent::create([
            'birth_year' => '1998',
            'user_id' => 2,
            'subscription_id' => 1
        ]);
    }
}
