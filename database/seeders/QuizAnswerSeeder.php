<?php

namespace Database\Seeders;

use App\Models\QuizAnswer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuizAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QuizAnswer::create([
            'quiz_question_id' => 1,
            'answer_choice' => 'Tono',
        ]);
        QuizAnswer::create([
            'quiz_question_id' => 1,
            'answer_choice' => 'Toni',
        ]);
        QuizAnswer::create([
            'quiz_question_id' => 1,
            'answer_choice' => 'Tina',
        ]);
        QuizAnswer::create([
            'quiz_question_id' => 1,
            'answer_choice' => 'Heru',
        ]);

        QuizAnswer::create([
            'quiz_question_id' => 2,
            'answer_choice' => 'Kambing',
        ]);
        QuizAnswer::create([
            'quiz_question_id' => 2,
            'answer_choice' => 'Kucing',
        ]);
        QuizAnswer::create([
            'quiz_question_id' => 2,
            'answer_choice' => 'Kancil',
        ]);
        QuizAnswer::create([
            'quiz_question_id' => 2,
            'answer_choice' => 'Sepeda',
        ]);
    }
}
