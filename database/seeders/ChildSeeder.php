<?php

namespace Database\Seeders;

use App\Models\Child;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChildSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Child::create([
            'name' => 'Putra',
            'gender' => 'Perempuan',
            'birth_date' => '2018-02-10',
            'parent_id' => 1,
            'avatar' => 'default.jpg'
        ]);

        Child::create([
            'name' => 'Putri',
            'gender' => 'Perempuan',
            'birth_date' => '2018-02-10',
            'parent_id' => 2,
            'avatar' => 'default.jpg'
        ]);

        Child::create([
            'name' => 'Agung',
            'gender' => 'Laki laki',
            'birth_date' => '2018-02-10',
            'parent_id' => 1,
            'avatar' => 'default.jpg'
        ]);

        Child::create([
            'name' => 'Brian',
            'gender' => 'Laki laki',
            'birth_date' => '2018-02-10',
            'parent_id' => 2,
            'avatar' => 'default.jpg'
        ]);

        Child::create([
            'name' => 'Rama',
            'gender' => 'Perempuan',
            'birth_date' => '2018-02-10',
            'avatar' => 'default.jpg'
        ]);
    }
}
