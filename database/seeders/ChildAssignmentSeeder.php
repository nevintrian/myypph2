<?php

namespace Database\Seeders;

use App\Models\ChildAssignment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChildAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChildAssignment::create([
            'child_id' => 1,
            'assignment_id' => 1,
            'total_assignment' => 2,
            'total_finish' => 0
        ]);
    }
}
