<?php

namespace Database\Seeders;

use App\Models\Badge;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Badge::create([
            'name' => 'Perjalanan Hebat',
            'description' => 'Mengarungi perjalanan hebat'
        ]);

        Badge::create([
            'name' => 'Petualangan',
            'description' => 'Menjalani petualangan hebat'
        ]);

        Badge::create([
            'name' => 'Imaginatif',
            'description' => 'Imaginasi yang hebat'
        ]);

        Badge::create([
            'name' => 'Edukasi',
            'description' => 'Suka belajar hal baru'
        ]);

        Badge::create([
            'name' => 'Hebat Berhitung',
            'description' => 'Berbakat berhitung'
        ]);

        Badge::create([
            'name' => 'Hebat Berbahasa',
            'description' => 'Pintar berbahasa'
        ]);

        Badge::create([
            'name' => 'Hebat Membaca',
            'description' => 'Pintar membaca'
        ]);
    }
}
