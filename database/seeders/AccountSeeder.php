<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Account::create([
            'email' => 'nevintrianade@gmail.com',
            'password' => 'nevin123',
            'phone_number' => '082234706515',
            'user_id' => 1
        ]);

        Account::create([
            'email' => 'admin@admin.com',
            'password' => 'password',
            'phone_number' => '081124342556',
            'user_id' => 2
        ]);

        Account::create([
            'email' => 'ne.vind99@gmail.com',
            'password' => 'ade123',
            'phone_number' => '089683162147',
            'user_id' => 3
        ]);
        Account::create([
            'email' => 'munih@gmail.com',
            'password' => 'munih123',
            'phone_number' => '08776578292',
            'user_id' => 4
        ]);
        Account::create([
            'email' => 'gunawan@gmail.com',
            'password' => 'gunawan123',
            'phone_number' => '089273817212',
            'user_id' => 5
        ]);

        Account::create([
            'email' => 'adeirawan@gmail.com',
            'password' => 'ade123',
            'phone_number' => '0823612734881',
            'user_id' => 6
        ]);
    }
}
