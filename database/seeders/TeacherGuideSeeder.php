<?php

namespace Database\Seeders;

use App\Models\TeacherGuide;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeacherGuideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeacherGuide::create([
            'title' => 'Petunjuk Guru 1',
            'content' => 'Ini adalah petunjuk 1'
        ]);
        TeacherGuide::create([
            'title' => 'Petunjuk Guru 2',
            'content' => 'Ini adalah petunjuk 2'
        ]);
        TeacherGuide::create([
            'title' => 'Petunjuk Guru 3',
            'content' => 'Ini adalah petunjuk 3'
        ]);
    }
}
