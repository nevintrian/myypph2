<?php

namespace Database\Seeders;

use App\Models\UserCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserCategory::create([
            'user_id' => 1,
            'category_id' => 1
        ]);
        UserCategory::create([
            'user_id' => 1,
            'category_id' => 2
        ]);
        UserCategory::create([
            'user_id' => 2,
            'category_id' => 3
        ]);
    }
}
