<?php

namespace Database\Seeders;

use App\Models\BookCollection;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookCollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookCollection::create([
            'collection_id' => 1,
            'book_id' => 1
        ]);

        BookCollection::create([
            'collection_id' => 1,
            'book_id' => 2
        ]);

        BookCollection::create([
            'collection_id' => 2,
            'book_id' => 3
        ]);

        BookCollection::create([
            'collection_id' => 2,
            'book_id' => 4
        ]);
    }
}
