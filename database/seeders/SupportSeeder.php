<?php

namespace Database\Seeders;

use App\Models\Support;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SupportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Support::create([
            'title' => 'Apa Itu Bukuaku.id ?',
            'content' =>
            '
                <p>Bukuaku.id adalah layanan buku berlangangan untuk anak anak berumur 3-14 thn yang berbasis online melalui website (www.bukuaku.id) atau melalui apliksai yang bisa didownload di Appstore atau Playstore . Bukuaku.id menyediakan buku buku digital dalam Bahasa Indonesia dan Bahasa English dari berbagai macam genre . Bukuaku.id juga memnyediakan buku interaktif dengan fitur “read to me”.</p>
                <p>Bukuaku.id mendukung tantangan dan semangat dari para pembaca kami dan berkomitmen untuk menyediakan koleksi buku-buku digital terbaik anak-anak untuk perjalanan membaca mereka.</p>

            '
        ]);
        Support::create([
            'title' => 'Cara Mendaftar Bukuaku.id ?',
            'content' =>
            '
                <ul>
                    <li>Pilih ‘Daftar Sekarang’</li>
                    <li>Masukan data informasi berupa email/nomor handphone dan password</li>
                    <li>Pilih metode penerimaan OTP</li>
                    <li>Masukkan OTP yang diterima</li>
                    <li>Pilih paket yang sesuai kebutuhan</li>
                    <li>Masukan methode pembayaran</li>
                    <li>Selesaikan transaksi</li>
                    <li>Akun Bukuaku.id anda sudah bisa digunakan</li>
                </ul>
            '
        ]);
        Support::create([
            'title' => 'Apakah Bukuaku.id aman untuk anak-anak ?',
            'content' => '<p>Bukuaku.id sangatlah aman di pakai oleh anak anak . Kami berkomitment untuk menyediakan buku buku yang aman untuk di baca oleh anak anak . Buku kami di pilih oleh tim guru dan professional untuk memastikan bahwa anaka anak tidak menemukan konten yang tidak pantas dalam website/aplikasi kami.</p>'
        ]);
        Support::create([
            'title' => 'Apakah sekolah bisa berlangganan bukuaku.id ?',
            'content' => '<p>Bukuaku.id menyediakan fitur untuk mempermudah sekolah memakai website/aplikasi untuk menyediakan buku-buku untuk murid , tujuan Bukuaku.id supaya guru guru bisa meningkatkan minat murid dalam membaca. Untuk program sekolah berlangganan , bisa hubungi kami di admin@bukuaku.id .</p>',
            'file' => 'http://youtube.com/abcde'
        ]);
        Support::create([
            'title' => 'Bagaimana jika lupa kata sandi di akun website/applikasi Bukuaku.id ?',
            'content' => '<p>Jika kamu lupa password, kamu bisa klik "Lupa Kata Sandi", kemudian kamu bisa isi nomor HP/email kamu yang terdaftar untuk verifikasi lebih lanjut supaya kamu bisa menggunakan kata sandi yang baru.</p>',
            'file' => 'gambar.jpg'
        ]);
    }
}
