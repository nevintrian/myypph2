<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'email' => 'nevintrianade@gmail.com',
            'password' => 'nevin123',
            'phone_number' => '082234706515',
            'name' => 'Nevin Trian',
            'role_id' => 1,
        ]);

        User::create([
            'email' => 'ne.vind99@gmail.com',
            'password' => 'ade123',
            'phone_number' => '089683162147',
            'name' => 'Ade Putera',
            'role_id' => 1,
        ]);

        User::create([
            'email' => 'munihdian@gmail.com',
            'password' => 'munih123',
            'phone_number' => '0823612734881',
            'name' => 'Munih Dian',
            'role_id' => 2,
        ]);

        User::create([
            'email' => 'ratihanggraeni@gmail.com',
            'password' => 'ratih123',
            'phone_number' => '0823612734881',
            'name' => 'Ratih Aggraeni',
            'role_id' => 2,
        ]);
    }
}
