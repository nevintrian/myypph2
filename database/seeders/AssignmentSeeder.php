<?php

namespace Database\Seeders;

use App\Models\Assignment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Assignment::create([
            'teacher_id' => 1,
            'name' => 'Membaca cerita petualangan',
            'deadline' => '2022-06-17',
            'is_draft' => 0
        ]);

        Assignment::create([
            'teacher_id' => 1,
            'name' => 'Belajar berhitung dan membaca',
            'deadline' => '2022-06-17',
            'is_draft' => 1
        ]);
    }
}
