<?php

namespace Database\Seeders;

use App\Models\Transaction;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::create([
            'parent_id' => 1,
            'subscription_id' => 3,
            'code' => 'TR001',
            'payment_method' => 'gopay',
            'description' => 'pembayaran standard subscription',
            'date_start' => '2022-02-01',
            'date_end' => '2023-02-01',
            'total_price' => 400000,
            'is_success' => 0,
        ]);

        Transaction::create([
            'parent_id' => 1,
            'subscription_id' => 3,
            'code' => 'TR002',
            'payment_method' => 'gopay',
            'description' => 'pembayaran standard subscription',
            'date_start' => '2022-05-01',
            'date_end' => '2023-05-01',
            'total_price' => 400000,
            'is_success' => 1,
            'payment_method' => 'gopay'
        ]);
    }
}
