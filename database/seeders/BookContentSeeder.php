<?php

namespace Database\Seeders;

use App\Models\BookContent;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BookContent::create([
            'book_id' => 1,
            'page' => 1
        ]);
        BookContent::create([
            'book_id' => 1,
            'page' => 2
        ]);

        BookContent::create([
            'book_id' => 2,
            'page' => 1
        ]);
        BookContent::create([
            'book_id' => 2,
            'page' => 2
        ]);

        BookContent::create([
            'book_id' => 3,
            'page' => 1
        ]);
        BookContent::create([
            'book_id' => 3,
            'page' => 2
        ]);

        BookContent::create([
            'book_id' => 4,
            'page' => 1
        ]);
        BookContent::create([
            'book_id' => 4,
            'page' => 2
        ]);
    }
}
