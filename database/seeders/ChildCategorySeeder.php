<?php

namespace Database\Seeders;

use App\Models\ChildCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChildCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChildCategory::create([
            'child_id' => 1,
            'category_id' => 1
        ]);
        ChildCategory::create([
            'child_id' => 1,
            'category_id' => 2
        ]);
        ChildCategory::create([
            'child_id' => 2,
            'category_id' => 3
        ]);
    }
}
