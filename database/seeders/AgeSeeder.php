<?php

namespace Database\Seeders;

use App\Models\Age;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Age::create([
            'min' => 0,
            'max' => 3
        ]);
        Age::create([
            'min' => 4,
            'max' => 5
        ]);
        Age::create([
            'min' => 6,
            'max' => 7
        ]);
        Age::create([
            'min' => 8,
            'max' => 9
        ]);
        Age::create([
            'min' => 10,
            'max' => 13
        ]);
    }
}
