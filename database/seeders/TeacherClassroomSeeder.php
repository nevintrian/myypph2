<?php

namespace Database\Seeders;

use App\Models\TeacherClassroom;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TeacherClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeacherClassroom::create([
            'teacher_id' => 1,
            'classroom_id' => 1,
        ]);
        TeacherClassroom::create([
            'teacher_id' => 1,
            'classroom_id' => 2,
        ]);
        TeacherClassroom::create([
            'teacher_id' => 2,
            'classroom_id' => 2,
        ]);
    }
}
