<?php

namespace Database\Seeders;

use App\Models\StudentAssignment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StudentAssignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudentAssignment::create([
            'student_id' => 1,
            'assignment_id' => 1,
            'total_assignment' => 2,
            'total_finish' => 0
        ]);

    }
}
