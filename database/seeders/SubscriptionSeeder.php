<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscription::create([
            'name' => 'Gratis',
            'price' => 0,
            'description' => 'Akses terbatas pada buku, max 20',
            'benefit' => 'Ini adalah keuntungan dari subscribe'
        ]);
        Subscription::create([
            'name' => 'Standard',
            'price' => 159000,
            'description' => 'Akses terbatas pada buku indonesia',
            'benefit' => 'Ini adalah keuntungan dari subscribe'
        ]);
        Subscription::create([
            'name' => 'Keluarga',
            'price' => 359000,
            'description' => 'Akses tak terbatas ke semua jenis buku, waktu membaca tak terbatas',
            'benefit' => 'Ini adalah keuntungan dari subscribe'
        ]);
        Subscription::create([
            'name' => 'Sekolah',
            'price' => 0,
            'description' => 'Akses tak terbatas ke semua jenis buku, waktu membaca tak terbatas',
            'benefit' => 'Ini adalah keuntungan dari subscribe'
        ]);
    }
}
