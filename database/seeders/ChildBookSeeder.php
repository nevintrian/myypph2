<?php

namespace Database\Seeders;

use App\Models\ChildBook;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChildBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChildBook::create([
            'book_id' => 1,
            'child_id' => 1,
            'current_page' => 100,
            'is_done' => 1,
            'is_favorite' => 1
        ]);

        ChildBook::create([
            'book_id' => 3,
            'child_id' => 1,
            'current_page' => 10,
            'is_done' => 0,
            'is_favorite' => 1
        ]);

        ChildBook::create([
            'book_id' => 3,
            'child_id' => 2,
            'current_page' => 100,
            'is_done' => 1,
            'is_favorite' => 1,
            'rating' => 5
        ]);
    }
}
