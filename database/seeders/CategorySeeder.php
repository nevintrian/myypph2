<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Adventure'
        ]);
        Category::create([
            'name' => 'Fable'
        ]);
        Category::create([
            'name' => 'Technology'
        ]);
        Category::create([
            'name' => 'Engineering'
        ]);
        Category::create([
            'name' => 'Math'
        ]);
        Category::create([
            'name' => 'Personal Development'
        ]);
        Category::create([
            'name' => 'Folklore'
        ]);
        Category::create([
            'name' => 'Myth'
        ]);
        Category::create([
            'name' => 'Non-fiction'
        ]);
        Category::create([
            'name' => 'Other'
        ]);
    }
}
