<?php

namespace Database\Seeders;

use App\Models\QuizQuestion;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuizQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QuizQuestion::create([
            'quiz_id' => 1,
            'question' => 'Siapa tokoh utama dalam cerita itu?',
            'correct_answer' => 'Tono'
        ]);

        QuizQuestion::create([
            'quiz_id' => 1,
            'question' => 'Siapa hewan yang berperan dalam cerita itu?',
            'correct_answer' => 'Kancil'
        ]);
    }
}
