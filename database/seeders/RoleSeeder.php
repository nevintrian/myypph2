<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role::create([
        //     'name' => 'admin',

        // ]);
        Role::create([
            'name' => 'parent',

        ]);
        // Role::create([
        //     'name' => 'child',

        // ]);
        Role::create([
            'name' => 'teacher',

        ]);
        // Role::create([
        //     'name' => 'student',
        // ]);
    }
}
