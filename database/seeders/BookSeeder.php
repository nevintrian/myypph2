<?php

namespace Database\Seeders;

use App\Models\Book;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::create([
            'author_id' => 1,
            'age_id' => 2,
            'title' => 'Petualangan Kancil',
            'synopsis' => 'Buku ini bercerita tentang petualangan kancil',
            'total_page' => 100,
            'total_time' => 60,
            'is_premium' => 1,
            'cover' => 'kancil.jpg'
        ]);
        Book::create([
            'author_id' => 2,
            'age_id' => 3,
            'title' => 'Petualangan Kuda',
            'synopsis' => 'Buku ini bercerita tentang petualangan kuda',
            'total_page' => 200,
            'total_time' => 120,
            'is_premium' => 0,
            'cover' => 'kuda.jpg'
        ]);
        Book::create([
            'author_id' => 3,
            'age_id' => 5,
            'title' => 'Belajar Berhitung',
            'synopsis' => 'Buku ini tentang belajar berhitung',
            'total_page' => 300,
            'total_time' => 360,
            'is_premium' => 0,
            'cover' => 'berhitung.jpg'
        ]);
        Book::create([
            'author_id' => 3,
            'age_id' => 5,
            'title' => 'Belajar Bahasa',
            'synopsis' => 'Buku ini tentang belajar bahasa',
            'total_page' => 300,
            'total_time' => 360,
            'is_premium' => 0,
            'cover' => 'bahasa.jpg'
        ]);
    }
}
