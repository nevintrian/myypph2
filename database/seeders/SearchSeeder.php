<?php

namespace Database\Seeders;

use App\Models\Search;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Search::create([
            'user_id' => 1,
            'keyword' => 'kancil'
        ]);

        Search::create([
            'user_id' => 1,
            'keyword' => 'kuda'
        ]);
        Search::create([
            'user_id' => 2,
            'keyword' => 'petualangan'
        ]);
        Search::create([
            'user_id' => 2,
            'keyword' => 'komik'
        ]);
    }
}
