<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create([
            'title' => 'Banner 1',
            'image' => 'banner1.jpg'
        ]);
        Banner::create([
            'title' => 'Banner 2',
            'image' => 'banner2.jpg'
        ]);
    }
}
