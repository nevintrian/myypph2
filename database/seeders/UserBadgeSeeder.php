<?php

namespace Database\Seeders;

use App\Models\UserBadge;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserBadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserBadge::create([
            'user_id' => 2,
            'badge_id' => 4
        ]);

        UserBadge::create([
            'user_id' => 2,
            'badge_id' => 5
        ]);
    }
}
