<?php

namespace Database\Seeders;

use App\Models\UserQuiz;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserQuizSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserQuiz::create([
            'quiz_id' => 1,
            'user_id' => 6,
            'correct_answer' => 1,
            'wrong_answer' => 1,
            'score' => 50
        ]);
    }
}
