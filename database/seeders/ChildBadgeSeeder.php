<?php

namespace Database\Seeders;

use App\Models\ChildBadge;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChildBadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChildBadge::create([
            'child_id' => 2,
            'badge_id' => 4
        ]);

        ChildBadge::create([
            'child_id' => 2,
            'badge_id' => 5
        ]);
    }
}
