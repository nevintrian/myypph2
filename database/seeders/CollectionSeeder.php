<?php

namespace Database\Seeders;

use App\Models\Collection;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Collection::create([
            'name' => 'Buku Petualangan',
            'teacher_id' => 1
        ]);

        Collection::create([
            'name' => 'Buku Edukasi',
            'teacher_id' => 1
        ]);
    }
}
