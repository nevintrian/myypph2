<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_assignments', function (Blueprint $table) {
            $table->id();
            $table->foreign('student_id')->references('id')->on('students');
            $table->foreignId('student_id');
            $table->foreign('assignment_id')->references('id')->on('assignments');
            $table->foreignId('assignment_id');
            $table->integer('total_assignment');
            $table->integer('total_finish');
            $table->date('date_finish')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_assignments');
    }
};
