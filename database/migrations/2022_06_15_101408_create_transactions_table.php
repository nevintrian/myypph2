<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id');
            $table->foreign('parent_id')->references('id')->on('parents');
            $table->foreignId('subscription_id');
            $table->foreign('subscription_id')->references('id')->on('subscriptions');
            $table->string('code');
            $table->string('payment_method');
            $table->string('description');
            $table->datetime('date_start');
            $table->datetime('date_end');
            $table->integer('total_price');
            $table->boolean('is_success');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
