<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignment_books', function (Blueprint $table) {
            $table->id();
            $table->foreign('assignment_id')->references('id')->on('assignments');
            $table->foreignId('assignment_id');
            $table->foreign('book_id')->references('id')->on('books');
            $table->foreignId('book_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignment_books');
    }
};
