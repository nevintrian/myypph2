<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->foreignId('author_id');
            $table->foreign('author_id')->references('id')->on('authors');
            $table->foreignId('age_id');
            $table->foreign('age_id')->references('id')->on('ages');
            $table->string('title');
            $table->text('synopsis');
            $table->integer('total_page');
            $table->integer('total_time');
            $table->boolean('is_premium');
            $table->string('cover');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
};
