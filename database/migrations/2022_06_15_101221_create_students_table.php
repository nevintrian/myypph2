<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->foreign('child_id')->references('id')->on('children');
            $table->foreignId('child_id');
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreignId('school_id');
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->foreignId('classroom_id');
            $table->string('student_number');
            $table->string('code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
