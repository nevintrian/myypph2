<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_content_translations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('book_content_id');
            $table->foreign('book_content_id')->references('id')->on('book_contents');
            $table->string('locale');
            $table->text('text');
            $table->string('image');
            $table->string('audio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_content_translations');
    }
};
