<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_assignments', function (Blueprint $table) {
            $table->id();
            $table->foreign('child_id')->references('id')->on('children');
            $table->foreignId('child_id');
            $table->foreign('assignment_id')->references('id')->on('assignments');
            $table->foreignId('assignment_id');
            $table->integer('total_assignment');
            $table->integer('total_finish');
            $table->date('date_finish')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_assignments');
    }
};
