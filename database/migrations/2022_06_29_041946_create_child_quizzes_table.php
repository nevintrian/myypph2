<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_quizzes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('quiz_id');
            $table->foreign('quiz_id')->references('id')->on('quizzes');
            $table->foreignId('child_id');
            $table->foreign('child_id')->references('id')->on('children');
            $table->integer('correct_answer');
            $table->integer('wrong_answer');
            $table->integer('score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_quizzes');
    }
};
