<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_badges', function (Blueprint $table) {
            $table->id();
            $table->foreign('book_id')->references('id')->on('books');
            $table->foreignId('book_id');
            $table->foreign('badge_id')->references('id')->on('badges');
            $table->foreignId('badge_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_badges');
    }
};
