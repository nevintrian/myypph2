<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_badges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('child_id');
            $table->foreign('child_id')->references('id')->on('children');
            $table->foreignId('badge_id');
            $table->foreign('badge_id')->references('id')->on('badges');
            // $table->integer('current_progress');
            // $table->boolean('is_done');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_badges');
    }
};
