<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('user_books', function (Blueprint $table) {
        //     $table->id();
        //     $table->foreignId('book_id');
        //     $table->foreign('book_id')->references('id')->on('books');
        //     $table->foreignId('user_id');
        //     $table->foreign('user_id')->references('id')->on('users');
        //     $table->integer('current_page');
        //     $table->boolean('is_done');
        //     $table->boolean('is_favorite');
        //     $table->integer('rating')->nullable();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_books');
    }
};
